CREATE TABLE dog
(
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    breed character varying(50) NOT NULL,
    date_of_birth timestamp without time zone,
    registration_date_time timestamp without time zone,
    vet_number character varying(50)
);

CREATE SEQUENCE DOG_SEQ
    MINVALUE 1
    MAXVALUE 9999999999
    INCREMENT BY 1
    START WITH 1
    CACHE 1;
