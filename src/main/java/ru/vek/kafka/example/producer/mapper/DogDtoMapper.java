package ru.vek.kafka.example.producer.mapper;

import org.mapstruct.AfterMapping;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import ru.vek.kafka.example.producer.dto.DogToKennelRequestDto;
import ru.vek.kafka.example.producer.dto.DogToKennelResponseDto;
import ru.vek.kafka.example.producer.entity.DogEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        collectionMappingStrategy = CollectionMappingStrategy.TARGET_IMMUTABLE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        componentModel = "spring")
public interface DogDtoMapper {

    String KENNEL_NAME = "Von Kennel August";

    DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yy");

    DogEntity convertToEntity(DogToKennelRequestDto source);

    DogToKennelResponseDto convertToDto(DogEntity source);

    @AfterMapping
    default void addAdditionalInfo(@MappingTarget DogEntity target) {
        var vetNumber = new StringBuilder();
        target.setName(target.getName() + " " + KENNEL_NAME);
        target.setVetNumber(vetNumber
                .append(LocalDate.now().format(DATE_TIME_FORMATTER))
                .append("-V_K_A-")
                .append("33")
                .toString());
        target.setRegistrationDateTime(LocalDateTime.now());
    }
}
