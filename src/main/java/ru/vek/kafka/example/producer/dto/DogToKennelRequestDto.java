package ru.vek.kafka.example.producer.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DogToKennelRequestDto {

    String name;

    String breed;

    LocalDate dateOfBirth;
}
