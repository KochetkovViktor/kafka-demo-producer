package ru.vek.kafka.example.producer.kafka;

public interface KafkaMessageSender {

    void sendMessageToKafka(String message);
}
