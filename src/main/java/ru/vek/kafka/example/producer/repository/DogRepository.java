package ru.vek.kafka.example.producer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.vek.kafka.example.producer.entity.DogEntity;


public interface DogRepository extends JpaRepository<DogEntity, Long> {
}
