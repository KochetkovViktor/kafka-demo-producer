package ru.vek.kafka.example.producer.dto;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DogToKennelResponseDto {

    String name;

    String breed;

    LocalDateTime registrationDateTime;

    String vetNumber;
}
