package ru.vek.kafka.example.producer.entity;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "dog")
@SequenceGenerator(name = "dogSequence", sequenceName = "DOG_SEQ", allocationSize = 1)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DogEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dogSequence")
    Long id;

    @Column
    String name;

    @Column
    String breed;

    @Column(name = "date_of_birth")
    LocalDate dateOfBirth;

    @Column(name = "registration_date_time")
    LocalDateTime registrationDateTime;

    @Column(name = "vet_number")
    String vetNumber;

}
