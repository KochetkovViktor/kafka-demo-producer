package ru.vek.kafka.example.producer.kafka.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import ru.vek.kafka.example.producer.kafka.KafkaMessageSender;

@Component
@Slf4j
public class KafkaMessageSenderImpl implements KafkaMessageSender {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${kafka.kennel.topic.name}")
    private String topic;

    @Override
    public void sendMessageToKafka(String message) {
        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topic, message);
        future.addCallback((success) -> {
        }, (error) -> {
            log.error(String.format("can't send message to failure topic cause error: %s", error.getMessage()));
            log.error(String.format("message:%s", message));
        });
        kafkaTemplate.flush();
    }
}
