package ru.vek.kafka.example.producer.service;

import ru.vek.kafka.example.producer.dto.DogToKennelRequestDto;
import ru.vek.kafka.example.producer.dto.DogToKennelResponseDto;

public interface DogKennelService {

    DogToKennelResponseDto addNewPuppyToKennel(DogToKennelRequestDto inputDto);
}
