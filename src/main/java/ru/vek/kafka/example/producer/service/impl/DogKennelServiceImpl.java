package ru.vek.kafka.example.producer.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vek.kafka.example.producer.dto.DogToKennelRequestDto;
import ru.vek.kafka.example.producer.dto.DogToKennelResponseDto;
import ru.vek.kafka.example.producer.kafka.KafkaMessageSender;
import ru.vek.kafka.example.producer.mapper.DogDtoMapper;
import ru.vek.kafka.example.producer.repository.DogRepository;
import ru.vek.kafka.example.producer.service.DogKennelService;

@Service
@Slf4j
@RequiredArgsConstructor
public class DogKennelServiceImpl implements DogKennelService {

    private final ObjectMapper objectMapper;
    private final DogDtoMapper mapper;
    @Autowired
    private DogRepository repository;
    @Autowired
    private KafkaMessageSender kafkaMessageSender;

    @Override
    public DogToKennelResponseDto addNewPuppyToKennel(DogToKennelRequestDto inputDto) {
        log.info("input : {}", inputDto);
        log.info("converted : {}", mapper.convertToEntity(inputDto));

        var response = repository.save(mapper.convertToEntity(inputDto));
        var responseDto = mapper.convertToDto(response);
        try {
            kafkaMessageSender.sendMessageToKafka(objectMapper.writeValueAsString(responseDto));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return responseDto;
    }
}
