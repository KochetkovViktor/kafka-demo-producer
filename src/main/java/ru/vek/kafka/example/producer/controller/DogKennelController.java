package ru.vek.kafka.example.producer.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.vek.kafka.example.producer.dto.DogToKennelRequestDto;
import ru.vek.kafka.example.producer.dto.DogToKennelResponseDto;
import ru.vek.kafka.example.producer.service.DogKennelService;

@RestController
@Api(value = "API для приема сообщений о собаках в питомнике",
        protocols = "http,https"
)
@RequestMapping("${urls.app.kennel.root}")
@RequiredArgsConstructor
@Slf4j
public class DogKennelController {

    private final DogKennelService service;

    @ApiOperation(value = "Размещение собак в питомнике")
    @PostMapping
    public ResponseEntity<DogToKennelResponseDto> addPuppyToKennel(@RequestBody DogToKennelRequestDto inputDto) {
        var responseDto = service.addNewPuppyToKennel(inputDto);
        return new ResponseEntity<>(responseDto, HttpStatus.OK);
    }
}
